# Snakemake pipeline




![poster](./Docs/dag.svg "DAG")

This Project is all about building a Snakemake pipeline. It is built for the dataprocessing course of the BioInformatics study ath the Hanze University of Applied Sciences. It is an attempt to recreate the pipeline as defined in ```pipeline.py```.

The pipeline is built with snakemake. The pipline can be used to map unknown samples to a known genome.

All code is kept up-to-date in this Git repository.

**NOTE: The log files etc are not included due to the diskquoata of my home folder, I will explain this in the oral exam**

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

**NOTE: THE PROGRAMS WITH A "*" SHOULD BE INSTALLED IN YOUR VIRTUAL ENVIROMENT**

Things you need to install the software and how to install them:

* Python 3 | [Python3 download page](https://www.python.org/downloads/)

* BWA* | [BWA downlaod page](https://sourceforge.net/projects/bio-bwa/files/)

* samtools* | [samtools downlaod page](http://www.htslib.org)

Install Python packages:

* Open a terminal window *(cmd.exe on windows) and type *


* Virtualenv
```
pip install virtualenv
```
and to make the venv: 
```
virtualenv -p /usr/bin/python3 venv
```

* Snakemake
```
pip install snakemake
```

### Installing

Now everything is installed, we can set up the web application in a development environment.

* Step 1 Set up the config file

* Edit the ```config.yaml``` to your needs.
    - The genome key is the path to the pathogene genome fasta file. This file should be BWA indexed by running ```bwa index <file>```.
    
    - The samples key contains all the sample files and the key is a placeholder ID.

* Step 2 Set up the environment

* Clone this repo

* Start a new terminal window

* Type in ```source venv/bin/activate```

* You have now an activated virtual enviroment

* Step 3 Run the snakefile

* In the same terminal window as where you just activated your virtual enviroment, type in the following: ```snakemake --snakefile Snakefile2 --cores 20```

    - _```--snakefile``` defines the snakefile to run, ```--cores``` defines the amount of threads used (this should equals the number in the ```Snakefile2```)._

## Built With

* [Snakemake](https://snakemake.readthedocs.io/en/stable/) - Pipeline framework
* [BWA](http://bio-bwa.sourceforge.net/) - Mapping algorithm
* [Samtools](http://www.htslib.org/) - Interactions with sequencing data
* [Python](https://www.python.org/) - Programming language on which Snakemake is build
* [Love](https://en.wikipedia.org/wiki/Love)

## Contributing

Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

---

## Versioning

We will use [semver](http://semver.org/) for versioning. But since this is the initial release of this software, there aren't any versions available yet.


## Authors

* **Bas Kasemir** - *Initial work* - [Portfolio](https://bioinf.nl/~bhnkasemir) | [Bitbucket](https://bitbucket.org/BasMonkey)

## Licenses

* This project is licensed under the MIT License. See the [LICENCE.md](LICENSE.md) file for further details.

## Acknowledgments

* Thanks to Willemijn Oudijk for staying at school aswell till 10 PM and continueing in the University Library till 12 PM that same evening

